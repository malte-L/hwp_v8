#include <iostream>
#include <b15f/b15f.h>
int main()
{
  B15F &drv = B15F::getInstance();
  drv.setRegister(&DDRA, 0x01);
  for (;;)
  {
    drv.setRegister(&PORTA, 1);
    drv.setRegister(&PORTA, 0);
  }
  return (0);
}