#include <iostream>
#include <b15f/b15f.h>

static void __print_bytes(FILE *f, long x, size_t n)
{
  fprintf(f, "%ld=\t\t\t", x);
  int i = n * 8 - 1;
  for (; i >= 0; i--)
  {
    if ((i + 1) % 4 == 0)
      fprintf(f, "_");
    if ((i + 1) % 8 == 0)
      fprintf(f, "_");
    if (x & (1 << i))
    {
      fprintf(f, "1");
    }
    else
    {
      fprintf(f, "0");
    }
  }
}

static void print_byte(unsigned char x)
{
  __print_bytes(stdout, x, sizeof(unsigned char));
}

int main()
{
  // senden
  B15F &drv = B15F::getInstance();
  drv.setRegister(&DDRA, 0x0);
  for (;;)
  {
    print_byte((char)drv.getRegister(&PINA));
    printf("\n");
  }

  return 0;
}