#include <iostream>
#include <b15f/b15f.h>

int main()
{
  // senden
  B15F &drv = B15F::getInstance();
  drv.setRegister(&DDRA, 0xFF);
  for (unsigned i = 0; i < 8; i++)
  {
    drv.setRegister(&PORTA, 1 << i);
  }

  return 0;
}