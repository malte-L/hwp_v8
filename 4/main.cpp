#include <iostream>
#include <b15f/b15f.h>
using namespace std;

B15F &drv = B15F::getInstance();

static void __print_bytes(FILE *f, long x, size_t n)
{
  fprintf(f, "%ld=\t\t\t", x);
  int i = n * 8 - 1;
  for (; i >= 0; i--)
  {
    if ((i + 1) % 4 == 0)
      fprintf(f, "_");
    if ((i + 1) % 8 == 0)
      fprintf(f, "_");
    if (x & (1 << i))
    {
      fprintf(f, "1");
    }
    else
    {
      fprintf(f, "0");
    }
  }
}

static void print_byte(unsigned char x)
{
  __print_bytes(stdout, x, sizeof(unsigned char));
  printf("\n");
}

char receive_byte()
{
  char msg = 0;

  drv.setRegister(&DDRA, 0);
  while (1)
  {
    char got = drv.getRegister(&PINA);
    msg = got & 0x7F;
    char control = got & 0x80;
    if (control != 0)
      break;
    printf("waiting for A7==1\n");
  }

  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTD, 0);

  return msg;
}

void send_byte(char msg)
{
  printf("sending: %c\n", msg);

  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTA, msg | 0x80);

  char res = drv.getRegister(&PINA);
  printf("waiting... PINA: ");
  print_byte(res);

  drv.setRegister(&DDRA, 0);
  while (1)
  {
    char res = drv.getRegister(&PINA);
    printf("waiting... PINA: ");
    print_byte(res);
    char control = res & 0x80;
    if (res == 0)
      break;
    printf("waiting for A7==0\n");
  }
}

int main()
{
  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTD, 0);

  drv.setRegister(&DDRA, 0);
  char is_blocked = drv.getRegister(&PINA) & 0x80;

  if (is_blocked)
  {
    printf("* Sender Online\n");
    printf("* Starte als Empfänger\n");

    //Bestätigung an Sender senden
    drv.setRegister(&DDRA, 0x40);  // Register zum Senden von Bit 7
    drv.setRegister(&PORTA, 0x40); //Bit 7 Senden

    while (drv.getRegister(&PINA) == 0x80)
    {
    }                           //Warten bis Sender Bit 8 nicht mehr sendet
    drv.setRegister(&PORTA, 0); // nicht mehr Senden
    printf("* Sender bereit\n");

    printf("\n");
    while (true) //Empfänger Starten
    {
      char got = receive_byte();
      if (got == '\0')
      {
        printf("got EOF\n");
        break;
      }
      printf("got: %c\n", got);
    }
  }
  else
  {
    printf("* Starte als Sender\n");

    drv.setRegister(&DDRA, 0x80);  // Register zum Senden von Bit 8
    drv.setRegister(&PORTA, 0x80); //Bit 8 Senden

    printf("* Warte auf Empfänger\n");
    while (drv.getRegister(&PINA) != 0xC0)
    {
    }                           //Warten bis Bit 7 vom Empfänger gesendet wird
    drv.setRegister(&PORTA, 0); //Bit 8 nicht mehr Senden

    printf("* Empfänger Online\n");

    printf("\n");
    unsigned char i = 0;
    while (true) //Sender Starten
    {
      printf("[%d]: ", i);

      if (i >= 16)
      {
        printf("sending EOF\n");
        send_byte('\0');
        i++;
      }
      else
      {
        send_byte(65 + i++);
      }

      if (i >= 18)
        break;
    }
  }
}
