#include <iostream>
#include <b15f/b15f.h>
using namespace std;

static void __print_bytes(FILE *f, long x, size_t n)
{
  fprintf(f, "%ld=\t\t\t", x);
  int i = n * 8 - 1;
  for (; i >= 0; i--)
  {
    if ((i + 1) % 4 == 0)
      fprintf(f, "_");
    if ((i + 1) % 8 == 0)
      fprintf(f, "_");
    if (x & (1 << i))
    {
      fprintf(f, "1");
    }
    else
    {
      fprintf(f, "0");
    }
  }
}

static void print_byte(unsigned char x)
{
  __print_bytes(stdout, x, sizeof(unsigned char));
  printf("\n");
}
//Ein Zeichen senden
void sendeZeichen(B15F &board, unsigned char zeichen)
{
  board.setRegister(&DDRA, 0xFF); //register auf schreiben setzen
  board.setRegister(&PORTA, zeichen);
}

//Ein Zeichen lesen
unsigned char leseZeichen(B15F &board)
{
  board.setRegister(&DDRA, 0x00); //register auf lesen setzen
  return board.getRegister(&PINA);
}

int main()
{
  //Board inti
  B15F &drv = B15F::getInstance();

  //Ausgabe Register Zurücksetzen
  sendeZeichen(drv, 0);

  std::cout << "" << std::endl;
  std::cout << "< Starte EtherChat für B15f Board >" << std::endl;
  std::cout << "-----------------------------------" << std::endl;
  std::cout << "- zum beenden 'EXIT' senden" << std::endl;
  std::cout << "- nur ASCII Zeichen senden" << std::endl;
  std::cout << "" << std::endl;

  std::cout << "* Prüfe Leitung" << std::endl;
  //Verbindung aushandeln
  if (leseZeichen(drv) == 128) //Wird Bit 8 gesendet?
  {
    //Ich bin Empfänger
    std::cout << "* Sender Online" << std::endl;
    std::cout << "* Starte als Empfänger" << std::endl;
    //std::cout << (int)leseZeichen(drv) << std::endl;

    //Bestätigung an Sender senden
    drv.setRegister(&DDRA, 0x40); // Register zum Senden von Bit 7
    drv.setRegister(&PORTA, 64);  //Bit 7 Senden
    //std::cout << (int)drv.getRegister(&PINA) << std::endl;
    while (drv.getRegister(&PINA) == 128)
    {
    }                           //Warten bis Sender Bit 8 nicht mehr sendet
    drv.setRegister(&PORTA, 0); // nicht mehr Senden
    std::cout << "* Sender bereit" << std::endl;

    std::cout << "" << std::endl;
    while (true) //Empfänger Starten
    {
      char msg = 0;

      drv.setRegister(&DDRA, 0);
      while (1)
      {
        char got = drv.getRegister(&PINA);
        msg = got & 0x7F;
        char control = got & 0x80;
        if (control != 0)
          break;
        printf("waiting for A7==1\n");
      }

      if (msg == '\0')
      {
        printf("got EOF");
        print_byte(msg);
        break;
      }

      printf("got: %c: ", msg);
      print_byte(msg);

      drv.setRegister(&DDRA, 0xFF);
      drv.setRegister(&PORTD, 0);

      // char in = leseZeichen(drv);
      // printf("%c", in);
      // if (in == '\0')
      //   break;
      // while (leseZeichen(drv) != '#')
      //   ;
    }
  }
  else
  {
    //Ich bin sender
    std::cout << "* Starte als Sender" << std::endl;
    //std::cout << (int)leseZeichen(drv) << std::endl;

    drv.setRegister(&DDRA, 0x80); // Register zum Senden von Bit 8
    drv.setRegister(&PORTA, 128); //Bit 8 Senden

    //std::cout << (int)drv.getRegister(&PINA) << std::endl;

    std::cout << "* Warte auf Empfänger" << std::endl;
    while (drv.getRegister(&PINA) != 192)
    {
    }                           //Warten bis Bit 7 vom Empfänger gesendet wird
    drv.setRegister(&PORTA, 0); //Bit 8 nicht mehr Senden

    std::cout << "* Empfänger Online" << std::endl;

    std::cout << "" << std::endl;
    unsigned char i = 0;
    while (true) //Sender Starten
    {
      // sendeZeichen(drv, 65 + i++);
      // sendeZeichen(drv, '#');

      char msg = (65 + i++);

      printf("sending: %c\n", msg);

      if (i >= 16)
      {
        printf("sending EOF");
        msg = '\0';
      }

      drv.setRegister(&DDRA, 0xFF);
      drv.setRegister(&PORTA, msg | 0x80);

      // drv.setRegister(&DDRA, 0x80);
      // drv.setRegister(&PORTA, 0x80);
      char res = drv.getRegister(&PINA);
      printf("waiting... PINA: ");
      print_byte(res);

      drv.setRegister(&DDRA, 0);
      while (1)
      {
        char res = drv.getRegister(&PINA);
        printf("waiting... PINA: ");
        print_byte(res);
        char control = res & 0x80;
        if (res == 0)
          break;
        printf("waiting for A7==0\n");
      }

      if (i == 18)
      {
        break;
      }
    }
  }
}
