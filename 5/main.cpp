#include <iostream>
#include <b15f/b15f.h>
using namespace std;

B15F &drv = B15F::getInstance();

static void __print_bytes(FILE *f, long x, size_t n)
{
  fprintf(f, "%ld=\t\t\t", x);
  int i = n * 8 - 1;
  for (; i >= 0; i--)
  {
    if ((i + 1) % 4 == 0)
      fprintf(f, "_");
    if ((i + 1) % 8 == 0)
      fprintf(f, "_");
    if (x & (1 << i))
    {
      fprintf(f, "1");
    }
    else
    {
      fprintf(f, "0");
    }
  }
}

static void print_byte(unsigned char x)
{
  __print_bytes(stdout, x, sizeof(unsigned char));
  printf("\n");
}

char receive_byte()
{
  char msg = 0;

  drv.setRegister(&DDRA, 0);
  while (1)
  {
    char got = drv.getRegister(&PINA);
    msg = got & 0x7F;
    char control = got & 0x80;
    if (control != 0)
      break;
    // printf("waiting for A7==1\n");
  }

  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTD, 0);

  return msg;
}

void send_byte(char msg)
{
  fprintf(stderr, "sending: %c\n", msg);

  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTA, msg | 0x80);

  char res = drv.getRegister(&PINA);
  fprintf(stderr, "waiting... PINA: ");

  drv.setRegister(&DDRA, 0);
  while (1)
  {
    char res = drv.getRegister(&PINA);
    char control = res & 0x80;
    if (control == 0)
      break;
    fprintf(stderr, "waiting for A7==0\n");
  }
}

// void send_message(char *msg)
// {
//   send_message(msg, (size_t)0xFFFFFFFF);
// }

void send_message(char *msg, size_t len)
{
  for (int i = 0; i < len; i++)
  {
    send_byte(msg[i]);
  }
}

const size_t BUFFER_SIZE = 1 << 10;

char *sender()
{
  size_t bufsize = BUFFER_SIZE;
  static char *buffer = (char *)malloc(bufsize * sizeof(char));
  size_t characters;

  if (buffer == NULL)
  {
    printf("ERR1\n");
    return NULL;
  }

  printf("> ");
  characters = getline(&buffer, &bufsize, stdin);

  if (characters == 0)
  {
    printf("ERR\n");
    return buffer;
  }
  buffer[characters++] = '\0';
  buffer[characters++] = '\0';
  send_message(buffer, characters);

  return buffer;
}

char *receiver()
{
  size_t bufsize = BUFFER_SIZE;
  static char *buffer = (char *)malloc(bufsize * sizeof(char));

  if (buffer == NULL)
  {
    printf("ERR1\n");
    return NULL;
  }

  fprintf(stderr, "waiting for msg...\n");
  int i = 0;
  for (; i < BUFFER_SIZE; i++)
  {
    buffer[i] = receive_byte();
    if (buffer[i] == '\0')
      break;

    fprintf(stderr, "got byte: %c\n", buffer[i]);
  }

  printf("< %s", buffer);

  return buffer;
}

int main()
{
  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTD, 0);

  drv.setRegister(&DDRA, 0);
  char is_blocked = drv.getRegister(&PINA) & 0x80;

  char *buffer = NULL, *r_buffer = NULL;

  if (is_blocked)
  {
    printf("* Sender Online\n");
    printf("* Starte als Empfänger\n");

    //Bestätigung an Sender senden
    drv.setRegister(&DDRA, 0x40);  // Register zum Senden von Bit 7
    drv.setRegister(&PORTA, 0x40); //Bit 7 Senden

    while (drv.getRegister(&PINA) == 0x80)
    {
    }                           //Warten bis Sender Bit 8 nicht mehr sendet
    drv.setRegister(&PORTA, 0); // nicht mehr Senden
    printf("* Sender bereit\n");

    printf("\n");

    for (;;)
    {
      r_buffer = receiver();
      buffer = sender();
    }
  }
  else
  {
    printf("* Starte als Sender\n");

    drv.setRegister(&DDRA, 0x80);  // Register zum Senden von Bit 8
    drv.setRegister(&PORTA, 0x80); //Bit 8 Senden

    printf("* Warte auf Empfänger\n");
    while (drv.getRegister(&PINA) != 0xC0)
    {
    }                           //Warten bis Bit 7 vom Empfänger gesendet wird
    drv.setRegister(&PORTA, 0); //Bit 8 nicht mehr Senden

    printf("* Empfänger Online\n");

    printf("\n");

    for (;;)
    {
      buffer = sender();
      r_buffer = receiver();
    }
  }

  free(buffer);
  free(r_buffer);
  return 0;
}
