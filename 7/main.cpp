/* kompiliert mit:  g++ -Wall -std=c++17 -O3 main.cpp -lb15fdrv -o main
** ausführen mit: ./main 2> /dev/null 
** */

#include <iostream>
#include <assert.h>
#include <b15f/b15f.h>
using namespace std;

B15F &drv = B15F::getInstance();

char receive_byte()
{
  char msg = 0;

  drv.setRegister(&DDRA, 0);
  while (1)
  {
    char got = drv.getRegister(&PINA);
    msg = got & 0x7F;
    char control = got & 0x80;
    if (control != 0)
      break;
    // printf("waiting for A7==1\n");
  }

  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTD, 0);

  return msg;
}

char unify_bytes(char b1, char b2, char b3)
{
  if (b1 == b2 || b1 == b3)
    return b1;
  if (b2 == b3)
    return b2;

  return -1;
}

void send_byte(char msg)
{
  //LOG(stderr, "sending: %c\n", msg);

  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTA, msg | 0x80);

  drv.setRegister(&DDRA, 0);
  while (1)
  {
    // drv.delay_ms(10);
    if ((drv.getRegister(&PINA) & 0x80) == 0x80)
    {
      continue;
      //LOG(stderr, "waiting for A7==0\n");
    }
    break;
  }
}

void send_message(char *msg, size_t len)
{
  fprintf(stderr, "sending message: ");

  for (unsigned i = 0; i < len; i++)
  {
    send_byte(msg[i]);
    fprintf(stderr, "%c", msg[i] == '\0' ? '?' : msg[i]);
  }

  fprintf(stderr, "\n");
}

char checksum(char *buffer, size_t len)
{
  char cs = 0;
  unsigned i = 0;
  for (i = 0; i < len; i++)
  {
    cs += buffer[i];
  }

  fprintf(stderr, "checksum for %d bytes\n", i);

  return cs;
}

const size_t BUFFER_SIZE = 1 << 10;

char *sender()
{
  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTA, 0);

  fprintf(stderr, "sender\n");

  size_t bufsize = BUFFER_SIZE;
  static char *buffer = (char *)malloc(bufsize * sizeof(char));
  size_t n;

  if (buffer == NULL)
  {
    printf("ERR1\n");
    return NULL;
  }

  fprintf(stderr, "sender 2\n");

  printf("> ");
  n = getline(&buffer, &bufsize, stdin);

  fprintf(stderr, "sender 3\n");

  if (n == 0)
  {
    printf("ERR\n");
    return buffer;
  }

  // buffer[n-1] == \n

  // fprintf(stderr, "buffer[n-1] = %c\n", buffer[n - 1]);
  // assert(buffer[n - 1] == '\0');

  fprintf(stderr, "sender 4\n");

  const char cs = checksum(buffer, n);
  buffer[n++] = cs;
  buffer[n++] = '\0';
  buffer[n++] = '\0';
  buffer[n++] = '\0';
  send_message(buffer, n);

  return buffer;
}

size_t receive_message(char *buffer, size_t len)
{
  unsigned i = 0;
  for (; i < BUFFER_SIZE; i++)
  {

    buffer[i] = receive_byte();

    fprintf(stderr, "in loop: %c\n", buffer[i]);

    if (buffer[i] == '\0')
    {
      break;
    }
  }

  for (; buffer[i] == '\0'; i--)
    ;

  fprintf(stderr, "got message %s", buffer);
  fprintf(stderr, "got_cs: %c\n", buffer[i]);
  // assert(buffer[i] == '#');

  char cs = checksum(buffer, i);
  if (buffer[i] != cs)
  {
    printf("mismatching checksums. got: %c, want: %c", cs, buffer[i]);
  }

  buffer[i] = '\0';

  return i;
}

char *receiver()
{
  fprintf(stderr, "receiver\n");

  size_t bufsize = BUFFER_SIZE;
  static char *buffer = (char *)malloc(bufsize * sizeof(char));

  fprintf(stderr, "receiver 2\n");

  if (buffer == NULL)
  {
    printf("ERR1\n");
    return NULL;
  }

  fprintf(stderr, "receiver 3\n");

  size_t n = receive_message(buffer, bufsize);
  if (n == bufsize)
  {
    fprintf(stderr, "msg too long");
    return buffer;
  }

  fprintf(stderr, "receiver 4\n");

  printf("< %s", buffer);

  return buffer;
}

int main()
{
  drv.setRegister(&DDRA, 0xFF);
  drv.setRegister(&PORTD, 0);

  drv.setRegister(&DDRA, 0);
  char is_blocked = drv.getRegister(&PINA) & 0x80;

  char *buffer = NULL, *r_buffer = NULL;

  if (is_blocked)
  {
    printf("* Sender Online\n");
    printf("* Starte als Empfänger\n");

    //Bestätigung an Sender senden
    drv.setRegister(&DDRA, 0x40);  // Register zum Senden von Bit 7
    drv.setRegister(&PORTA, 0x40); //Bit 7 Senden
    // drv.delay_ms(10);

    while (drv.getRegister(&PINA) == 0x80)
    {
    }                           //Warten bis Sender Bit 8 nicht mehr sendet
    drv.setRegister(&PORTA, 0); // nicht mehr Senden
    printf("* Sender bereit\n");

    printf("\n");

    for (;;)
    {
      r_buffer = receiver();
      buffer = sender();
    }
  }
  else
  {
    printf("* Starte als Sender\n");

    drv.setRegister(&DDRA, 0x80);  // Register zum Senden von Bit 8
    drv.setRegister(&PORTA, 0x80); //Bit 8 Senden

    // drv.delay_ms(10);
    printf("* Warte auf Empfänger\n");
    while (drv.getRegister(&PINA) != 0xC0)
    {
    }                           //Warten bis Bit 7 vom Empfänger gesendet wird
    drv.setRegister(&PORTA, 0); //Bit 8 nicht mehr Senden

    printf("* Empfänger Online\n");

    printf("\n");

    for (;;)
    {
      buffer = sender();
      r_buffer = receiver();
    }
  }

  free(buffer);
  free(r_buffer);
  return 0;
}
